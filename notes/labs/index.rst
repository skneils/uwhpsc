
.. _labs:

Notes to accompany lab sessions
=========================================

.. toctree::
   :maxdepth: 1

   lab1
   lab8
   lab9
   lab10
   lab11
   lab12
   lab13

See also the codes in `$UWHPSC/labs`.
